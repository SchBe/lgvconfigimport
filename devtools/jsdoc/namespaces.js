/**
 * @namespace Themes
 * @memberOf GFI
 * @description GFI-Themes
 */

/**
 * @namespace ContiniuousCountingBikeTheme
 * @memberOf GFI.Themes
 * @description GFI Theme for ContiniuousCountingBike
 */

/**
 * @namespace MapMarker
 * @description MapMarker
 */

/**
 * @namespace GFI
 * @description Get Feature Info
 */

/**
 * @namespace Title
 * @description Portal title
 */

/**
 * @namespace Quickhelp
 * @description fooBar
 */

/**
 * @namespace Util
 * @description fooBar
 */

/**
 * @namespace Menu
 * @description Definition of menu
 */

/**
 * @namespace Searchbar
 * @description fooBar
 */

/**
 * @namespace Alerting
 * @description Alerting system that responds to given events.
 * Used to have same alert all over the portal.
 */

/**
 * @namespace Contact
 * @description Contact Formular that allows the user to generate and send an email to the defined email account.
 */

/**
 * @namespace App
 * @description Loads core and instances all modules that have to be loaded, due to portal config
 */

/**
 * @namespace ClickCounter
 * @description This functionality registers user click behaviour and updates the url of an created iframe
 * used for Verkehrsportal.
 */

/**
 * @namespace ModelList
 * @description Module to gather all Models
 */

/**
 * @namespace StyleWMS
 * @description Tool that can modify wms tiles on request using an SLD-BODY
 */

/**
 * @namespace Graph
 * @description Module for generating graphics via d3
 */

/**
 * @namespace Main
 * @description Loading basic requirements such as the css. Then it starts the App.
 */

/**
 * @namespace Backbone
 * @description Backbone library for code architecture
 */

/**
 * @class Model
 * @memberOf Backbone
 * @description {@link http://backbonejs.org/#Model}
 */

/**
 * @class View
 * @memberOf Backbone
 * @description {@link http://backbonejs.org/#View}
 */

/**
 * @class Collection
 * @memberOf Backbone
 * @description {@link http://backbonejs.org/#Collection}
 */
